<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bridegroom;
use File;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\BridegroomImage;
use App\Models\BridegroomWish;

class BridegroomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bridegrooms = Bridegroom::all();
        return view("dashboard.bridegrooms.index",compact(["bridegrooms"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("dashboard.bridegrooms.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        //dd($input);
        $bridegroom = new Bridegroom;                
        $bridegroom->bride_name = $input['bride_name'];                     
        $bridegroom->bride_qoute = $input['bride_qoute'];                     
        $bridegroom->bride_instagram = $input['bride_instagram'];                             
        $files = $input['bride_photo'];
        if ($files) {
            $destinationPath    = 'uploads/attachment/bridegrooms/'; // The destination were you store the document.
            if(!(file_exists(public_path('/uploads/attachment/bridegrooms/'))))
            {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
            $mime_type          = $files->getMimeType(); // Gets this example image/png
            $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
            $filename           = time().'-'.$filename; // random file name to replace original
            $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
            $bridegroom->bride_photo = $destinationPath.'/'.$filename;
        }
        $bridegroom->groom_name = $input['groom_name'];
        $bridegroom->groom_qoute = $input['groom_qoute'];
        $bridegroom->groom_instagram = $input['groom_instagram'];
        $files = $input['groom_photo'];
        if ($files) {
            $destinationPath    = 'uploads/attachment/bridegrooms/'; // The destination were you store the document.
            if(!(file_exists(public_path('/uploads/attachment/bridegrooms/'))))
            {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
            $mime_type          = $files->getMimeType(); // Gets this example image/png
            $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
            $filename           = time().'-'.$filename; // random file name to replace original
            $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
            $bridegroom->groom_photo = $destinationPath.'/'.$filename;
        }
        $bridegroom->qoute = $input['qoute'];
        $bridegroom->qoute_detail = $input['qoute_detail'];
        //$meeting->wedding_date = date("Y-m-d", strtotime($input['wedding_date']));
        $bridegroom->location_weeding = $input['location_weeding'];
        //$bridegroom->location_lat = $input['location_lat'];
        //$bridegroom->location_lang = $input['location_lang'];
        $files = $input['image_slider'];
        if ($files) {
            $destinationPath    = 'uploads/attachment/bridegrooms/'; // The destination were you store the document.
            if(!(file_exists(public_path('/uploads/attachment/bridegrooms/'))))
            {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
            $mime_type          = $files->getMimeType(); // Gets this example image/png
            $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
            $filename           = time().'-'.$filename; // random file name to replace original
            $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
            $bridegroom->image_slider = $destinationPath.'/'.$filename;
        }
        $bridegroom->slug = Str::slug($input['slug'], '-');
        
        $bridegroom->save();
        \Session::flash('success','Data RS/Poli Rujukan berhasil dibuat');
        return redirect("dashboard/bridegrooms/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bridegroom = Bridegroom::find($id);
        return view("dashboard.bridegrooms.show",compact(["bridegroom"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bridegroom = Bridegroom::find($id);
        return view("dashboard.bridegrooms.edit",compact(["bridegroom"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        //dd($input);
        $bridegroom = Bridegroom::find($id);                
        $bridegroom->bride_name = $input['bride_name'];                     
        $bridegroom->bride_qoute = $input['bride_qoute'];                     
        $bridegroom->bride_instagram = $input['bride_instagram'];                             
        if ($request->file('bride_photo')) {
            $files = $input['bride_photo'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/bridegrooms/'; // The destination were you store the document.
                if(!(file_exists(storage_path('/uploads/attachment/bridegrooms/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $bridegroom->bride_photo = $destinationPath.'/'.$filename;
            }
        }
        $bridegroom->groom_name = $input['groom_name'];
        $bridegroom->groom_qoute = $input['groom_qoute'];
        $bridegroom->groom_instagram = $input['groom_instagram'];
        if ($request->file('groom_photo')) {
            $files = $input['groom_photo'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/bridegrooms/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/bridegrooms/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $bridegroom->groom_photo = $destinationPath.'/'.$filename;
            }
        }
        $bridegroom->qoute = $input['qoute'];
        $bridegroom->qoute_detail = $input['qoute_detail'];
        //$meeting->wedding_date = Carbon::createFromFormat('d/m/Y',$input['wedding_date']);
        $bridegroom->location_weeding = $input['location_weeding'];
        $bridegroom->location_lat = $input['location_lat'];
        $bridegroom->location_long = $input['location_long'];
        if ($request->file('image_slider')) {
            $files = $input['image_slider'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/bridegrooms/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/bridegrooms/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $bridegroom->image_slider = $destinationPath.'/'.$filename;
            }
        }
        $bridegroom->slug = Str::slug($input['slug'], '-');
        
        $bridegroom->save();
        \Session::flash('success','Data RS/Poli Rujukan berhasil dibuat');
        return redirect("dashboard/bridegrooms/");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_gallery()
    {        
        return view("dashboard.bridegrooms.create_gallery");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function gallery(Request $request)
    {
        $input = $request->all();        
        $length = 16;
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $strrandom =  substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
        
        $bridegroom_image = new BridegroomImage;                
        $bridegroom_image->bridegroom_id = $input['bridegroom_id'];                                             
        $files = $input['image'];
        if ($files) {
            $destinationPath    = 'uploads/attachment/bridegroom_gallery/'; // The destination were you store the document.
            if(!(file_exists(public_path('/uploads/attachment/bridegroom_gallery/'))))
            {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            $filename           = $strrandom; // Original file name that the end user used for it.
            $mime_type          = $files->getMimeType(); // Gets this example image/png
            $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
            $filename           = time().'-'.$filename; // random file name to replace original
            $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
            $bridegroom_image->image = $destinationPath.'/'.$filename;
        }                
        $bridegroom_image->save();
        \Session::flash('success','Data RS/Poli Rujukan berhasil dibuat');
        return redirect("dashboard/bridegrooms/".$bridegroom_image->bridegroom_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function bridegroom_images_delete($id)
    {
        $bridegroom_image = BridegroomImage::find($id);
        if($bridegroom_image->image){
            // remove image
            unlink($bridegroom_image->image);
        }
        $bridegroom_image->delete();
        \Session::flash('success','Dokumen berhasil dihapus');
        return redirect("dashboard/bridegrooms/".$bridegroom_image->bridegroom_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_wish()
    {        
        return view("dashboard.bridegrooms.create_wish");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function wish(Request $request)
    {
        $input = $request->all();

        $bridegroom_wish = new BridegroomWish;                
        $bridegroom_wish->name = $input['name'];                                                                    
        $bridegroom_wish->bridegroom_id = $input['bridegroom_id'];                                                                    
        $bridegroom_wish->email = $input['email'];                                                                    
        $bridegroom_wish->wish = $input['wish'];                                                                    
        $bridegroom_wish->save();
        \Session::flash('success','Wish berhasil dibuat');
        return redirect("dashboard/bridegrooms/".$bridegroom_wish->bridegroom_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function bridegroom_wish_delete($id)
    {
        $bridegroom_wish = BridegroomWish::find($id);
        $bridegroom_wish->delete();
        \Session::flash('success','Dokumen berhasil dihapus');
        return redirect("dashboard/bridegrooms/".$bridegroom_wish->bridegroom_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
