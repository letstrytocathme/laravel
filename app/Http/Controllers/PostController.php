<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

use File;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view("dashboard.posts.index",compact(["posts"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("dashboard.posts.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $random = Str::random(5);
        
        $post = new Post;                
        $post->title = $input['title'];                     
        $post->body = $input['body'];                     
        $post->created_by = $input['created_by'];                     
        if(isset($input['image'])){
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/posts/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/posts/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $post->image = $destinationPath.'/'.$filename;
            }
        }       
        $post->post_date = date("Y-m-d", strtotime($input['post_date']));        
        $post->slug = Str::slug($input['title'].'-'.$random, '-');
        
        $post->save();
        \Session::flash('success','Data Post berhasil dibuat');
        return redirect("dashboard/posts/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view("dashboard.posts.show",compact(["post"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view("dashboard.posts.edit",compact(["post"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        //dd($input);
        $post = Post::find($id);           
        $post->title = $input['title'];                     
        $post->body = $input['body'];   
        $post->updated_by = $input['updated_by'];
        if(isset($input['image'])){
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/posts/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/posts/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $post->image = $destinationPath.'/'.$filename;
            }
        }       
        $post->post_date = date("Y-m-d", strtotime($input['post_date']));
        $post->status_id = $input['status_id'];
        
        $post->save();
        \Session::flash('success','Data Post berhasil dibuat');
        return redirect("dashboard/posts/");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if(isset($post->image)){
            // remove image
            unlink($post->image);
        }
        $post->delete();
        \Session::flash('success','Post berhasil dihapus');
        return redirect("dashboard/posts/");
    }
}
