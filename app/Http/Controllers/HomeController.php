<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Bridegroom;
use App\Models\BridegroomWish;
use App\Models\Post;

use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {        
        $date_now = Carbon::now();
        $posts = Post::where('status_id', 1)->where( 'post_date', '<=', date('Y-m-d', strtotime($date_now)) )->get();
        return view('home',compact(["posts"]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function invitation($slug)
    {                        
        $bridegroom = Bridegroom::where('slug', $slug)->first();
                
        if(is_null($bridegroom)){
        	return redirect("/");
        } else {
        	return view('invitations.index',compact(["bridegroom"]));
        }        
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function wish(Request $request)
    {
        $input = $request->all();

        $bridegroom_wish = new BridegroomWish;                
        $bridegroom_wish->name = $input['name'];                                                                    
        $bridegroom_wish->bridegroom_id = $input['bridegroom_id'];                                                                    
        $bridegroom_wish->email = $input['email'];                                                                    
        $bridegroom_wish->wish = $input['wish'];                                                                    
        $bridegroom_wish->attending = $input['attending'];                                                                    
        $bridegroom_wish->session_id = $input['session_id'];                                                                            
        $bridegroom_wish->save();
        \Session::flash('success','Wish berhasil dibuat');
        return redirect("cerita/".$bridegroom_wish->bridegroom->slug);
    }
        
}
