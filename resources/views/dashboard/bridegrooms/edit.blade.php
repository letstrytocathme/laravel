@extends('layouts.dashboard')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Edit Data Pengantin Pria dan Wanita</h3>
    </div>
    <div class="panel-body">
        <div class="row">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bridegrooms.update', $bridegroom->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="col-lg-6">
                <div class="margin-bottom-50">                                        
                    <h4>Data Pengantin Wanita</h4>
                    <br />
                    <!-- Horizontal Form -->     
                                                       
                        <input type="hidden" class="form-control" name="created_by" id="created_by" value="{{ Auth::user()->id }}">
                         @csrf                        
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Nama</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_name" type="text" class="form-control" name="bride_name" value="{{ $bridegroom->bride_name }}" placeholder="Input Nama" autofocus >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Qoute</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_qoute" type="text" class="form-control" name="bride_qoute" value="{{ $bridegroom->bride_name }}"  placeholder="Input Nama" autofocus >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Instagram</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_instagram" type="text" class="form-control" name="bride_instagram" value="{{ $bridegroom->bride_instagram }}"  placeholder="Input Nama" autofocus >
                            </div>
                        </div>
                        @if($bridegroom->bride_photo)
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Foto</label>
                            </div>
                            <div class="col-md-4">
                                <img id="bride_photo" src="/{{$bridegroom->bride_photo}}" alt="{{$bridegroom->name}}" style="width:100%;max-width:300px">                                
                            </div>
                            <div class="col-md-4">
                                <input id="bride_photo" type="file" class="form-control{{ $errors->has('bride_photo') ? ' is-invalid' : '' }}" name="bride_photo"  placeholder="Input Gambar Sidang" autofocus>

                                @if ($errors->has('bride_photo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bride_photo') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-8">
                                <p class="text-red">* Upload jika ingin merubah gambar sidang</p>
                                <p class="text-red">* Ukuran Maksimal File 1MB</p>
                                <p class="text-red">* Format File : jpeg, jpg, png</p>
                            </div>
                        </div>                        
                        @else
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Gambar Sidang</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_photo" type="file" class="form-control{{ $errors->has('bride_photo') ? ' is-invalid' : '' }}" name="bride_photo"  placeholder="Input Gambar Sidang" autofocus>

                                @if ($errors->has('bride_photo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bride_photo') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4"></div>
                            <div class="col-xs-2">
                                <p class="text-red">* Upload jika ingin merubah gambar sidang</p>
                                <p class="text-red">* Ukuran Maksimal File 1MB</p>
                                <p class="text-red">* Format File : jpeg, jpg, png</p>
                            </div>
                        </div>
                        @endif
                                                                                                                                       
                </div>
            </div>
            <div class="col-lg-6">
                <div class="margin-bottom-50">                    
                    <h4>Data Pengantin Pria</h4>
                    <br />
                    <!-- Horizontal Form -->                                           
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Nama</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_name" type="text" class="form-control" name="groom_name" value="{{ $bridegroom->groom_name }}" placeholder="Input Nama" autofocus >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Qoute</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_qoute" type="text" class="form-control" name="groom_qoute" value="{{ $bridegroom->groom_qoute }}" placeholder="Input Nama" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Instagram</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_instagram" type="text" class="form-control" name="groom_instagram" value="{{ $bridegroom->groom_instagram }}"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>
                    @if($bridegroom->groom_photo)
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Foto</label>
                        </div>
                        <div class="col-md-4">
                            <img id="groom_photo" src="/{{$bridegroom->groom_photo}}" alt="{{$bridegroom->name}}" style="width:100%;max-width:300px">                                
                        </div>
                        <div class="col-md-4">
                            <input id="groom_photo" type="file" class="form-control{{ $errors->has('groom_photo') ? ' is-invalid' : '' }}" name="groom_photo"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('groom_photo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('groom_photo') }}</strong>
                                </span>
                            @endif
                        </div>                            
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-8">
                            <p class="text-red">* Upload jika ingin merubah gambar sidang</p>
                            <p class="text-red">* Ukuran Maksimal File 1MB</p>
                            <p class="text-red">* Format File : jpeg, jpg, png</p>
                        </div>
                    </div>                        
                    @else
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Gambar Sidang</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_photo" type="file" class="form-control{{ $errors->has('groom_photo') ? ' is-invalid' : '' }}" name="groom_photo"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('groom_photo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('groom_photo') }}</strong>
                                </span>
                            @endif
                        </div>                            
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4"></div>
                        <div class="col-xs-2">
                            <p class="text-red">* Upload jika ingin merubah gambar sidang</p>
                            <p class="text-red">* Ukuran Maksimal File 1MB</p>
                            <p class="text-red">* Format File : jpeg, jpg, png</p>
                        </div>
                    </div>
                    @endif                                                                                                           
                </div>
            </div>
            <div class="col-lg-12">
                <div class="margin-bottom-50">                    
                    <!-- <h4>Data Pengantin Pria</h4> -->
                    <br />
                    <!-- Horizontal Form -->                                           
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Qoute Center</label>
                        </div>
                        <div class="col-md-8">
                            <input id="qoute" type="text" class="form-control" name="qoute" value="{{ $bridegroom->qoute }}" placeholder="Input Nama" autofocus >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Qoute Detail</label>
                        </div>
                        <div class="col-md-8">
                            <input id="qoute_detail" type="text" class="form-control" name="qoute_detail" value="{{ $bridegroom->qoute_detail }}" placeholder="Input Nama" autofocus >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Tanggal Acara Pernikahan</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                    <label class="input-group datepicker-only-init">
                                        <input type="text" id="wedding_date"  class="form-control datepicker-only-init" name="wedding_date" value="{{ $bridegroom->wedding_date }}" placeholder="Input Tanggal Lahir" autofocus/>
                                        <span class="input-group-addon">
                                            <i class="icmn-calendar"></i>
                                        </span>
                                    </label>
                                </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Lokasi Acara Pernikahan</label>
                        </div>
                        <div class="col-md-8">
                            <input id="location_weeding" type="text" class="form-control" name="location_weeding" value="{{ $bridegroom->location_weeding }}" placeholder="Input Nama" autofocus >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Lat</label>
                        </div>
                        <div class="col-md-4">
                            <input id="location_lat" type="text" class="form-control" name="location_lat" value="{{ $bridegroom->location_lat }}" placeholder="Input Nama" autofocus >
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Long</label>
                        </div>
                        <div class="col-md-4">
                            <input id="location_long" type="text" class="form-control" name="location_long" value="{{ $bridegroom->location_long }}" placeholder="Input Nama" autofocus >
                        </div>                       
                    </div>    
                    @if($bridegroom->image_slider)
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Image Slider Utama</label>
                        </div>
                        <div class="col-md-4">
                            <img id="image_slider" src="/{{$bridegroom->image_slider}}" alt="{{$bridegroom->name}}" style="width:100%;max-width:300px">                                
                        </div>
                        <div class="col-md-4">
                            <input id="image_slider" type="file" class="form-control{{ $errors->has('image_slider') ? ' is-invalid' : '' }}" name="image_slider"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('image_slider'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image_slider') }}</strong>
                                </span>
                            @endif
                        </div>                            
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-8">
                            <p class="text-red">* Upload jika ingin merubah gambar sidang</p>
                            <p class="text-red">* Ukuran Maksimal File 1MB</p>
                            <p class="text-red">* Format File : jpeg, jpg, png</p>
                        </div>
                    </div>                        
                    @else
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Image Slider Utama</label>
                        </div>
                        <div class="col-md-8">
                            <input id="image_slider" type="file" class="form-control{{ $errors->has('image_slider') ? ' is-invalid' : '' }}" name="image_slider"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('image_slider'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image_slider') }}</strong>
                                </span>
                            @endif
                        </div>                            
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4"></div>
                        <div class="col-xs-2">
                            <p class="text-red">* Upload jika ingin merubah gambar sidang</p>
                            <p class="text-red">* Ukuran Maksimal File 1MB</p>
                            <p class="text-red">* Format File : jpeg, jpg, png</p>
                        </div>
                    </div>
                    @endif                                       
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Slug Request</label>
                        </div>
                        <div class="col-md-8">
                            <input id="slug" type="text" class="form-control" name="slug" value="{{ $bridegroom->slug }}"  placeholder="Input Nama" autofocus >
                        </div>
                    </div>                                                     
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Update') }}
                            </button>
                            <a class="btn btn-success" href="/meetings">Back</a>
                        </div>
                    </div>                                                                                          
                </div>
            </div>

            
        </form>
        </div>        
</section>
<!-- End -->
<script>
    $(function(){
        $('.select2').select2();

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'd/m/Y'
        });
    })
</script>
@endsection