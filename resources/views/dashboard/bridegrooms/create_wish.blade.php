@extends('layouts.dashboard')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Form Data Pengantin Pria dan Wanita</h3>
    </div>
    <br />
    <div class="panel-body">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/dashboard/wish') }}" enctype="multipart/form-data">
        <div class="row">            
            <div class="col-lg-12">
                <div class="margin-bottom-50">                    
                    <!-- <h4>Data Pengantin Pria</h4> -->
                    <br />
                    <!-- Horizontal Form -->                                                               
                    @csrf
                    <input type="hidden" class="form-control" name="bridegroom_id" id="bridegroom_id" value="{{ $_GET['bridegroom_id'] }}">
                    <div class="modal-body">                                    
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Nama</label>
                            </div>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name"  placeholder="Input Nama" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Email</label>
                            </div>
                            <div class="col-md-8">
                                <input id="email" type="text" class="form-control" name="email"  placeholder="Input Email" autofocus>
                            </div>
                        </div>                                  
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Your Wish</label>
                            </div>
                            <div class="col-md-8">
                                <textarea id="wish" class="form-control" name="wish" placeholder="Your Wish" autofocus></textarea>
                            </div>
                        </div>                                                          
                    </div>                                                                                                                                         
                </div>
            </div>
        </div>  

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Create') }}
                </button>
                <a class="btn btn-success" href="/dashboard/bridegrooms/">Back</a>
            </div>
        </div>
        </form>
    </div>
    <!-- End Horizontal Form -->      
</section>
<!-- End -->
<script>
    $(function(){
        $('.select2').select2();
    })
</script>
@endsection