<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBridegroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bridegrooms', function (Blueprint $table) {
            $table->id();
            $table->string('bride_name')->nullable();
            $table->string('bride_qoute')->nullable();
            $table->string('bride_instagram')->nullable();
            $table->string('bride_facebook')->nullable();
            $table->string('bride_twitter')->nullable();
            $table->string('bride_photo')->nullable();
            $table->string('groom_name')->nullable();
            $table->string('groom_qoute')->nullable();
            $table->string('groom_instagram')->nullable();
            $table->string('groom_facebook')->nullable();
            $table->string('groom_twitter')->nullable();
            $table->string('groom_photo')->nullable();
            $table->text('qoute')->nullable();
            $table->text('qoute_detail')->nullable();
            $table->date('wedding_date')->nullable();
            $table->string('location_weeding')->nullable();
            $table->string('location_lat')->nullable();
            $table->string('location_long')->nullable();
            $table->string('image_slider')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bridegrooms');
    }
}
